<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');

    }

    public function getCompany(Request $r)
    {
        $query = DB::table('company');
        $r->input('House') == 1 ? $query->where('house', 1) : '';
        $r->input('Car') == 1 ? $query->where('car', 1) : '';
        $r->input('Bike') == 1 ? $query->where('bike', 1) : '';
        $r->input('License') == 1 ? $query->where('license', 1) : '';
        $r->input('Insurance') == 1 ? $query->where('insurance', 1) : '';
        if (!empty($r->input('seachInp'))) {
            $query->where('company_name', 'LIKE', '%' . $r->input('seachInp') . '%'); 
        }
        $result = $query->get();
        return response()->json($result);
    }
}
