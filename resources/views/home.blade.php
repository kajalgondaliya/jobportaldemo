    @extends('app')
    @section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
      <h2>Fetched User</h2>
      <div class="table-responsive">
        <table class="table table-striped table-sm" id="companyTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Company Name</th>
              <th>Is house Required</th>
              <th>Is car Required</th>
              <th>Is bike Required</th>
              <th>Is license Required</th>
              <th>Is insurance Required</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
</table>
</div>
</main>


<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function onchangeSearch() {
        getCompany();
    }
    getCompany();
    function getCompany() {
        var seachInp = $("#seachInp").val();
        var IsHouse = $("#house").is(':checked') ? 1 : 0;
        var IsCar = $("#car").is(':checked') ? 1 : 0;
        var IsBike = $("#bike").is(':checked') ? 1 : 0;
        var IsLicense = $("#license").is(':checked') ? 1 : 0;
        var IsInsurance = $("#insurance").is(':checked') ? 1 : 0;

        $.ajax({
            url: APP_URL+'/getCompany',
            type: 'POST',
            dataType: 'json',
            data: {seachInp, seachInp,
                House : IsHouse , Car : IsCar , Bike : IsBike , License : IsLicense , Insurance : IsInsurance},
            success: function(data){
                $('#companyTable Tbody').html('');
                var html = '';
                var j  = 1;
                if (data.length == 0) {
                    html = '<tr><td><td></td></td><td></td><td>No data found</td><td></td><td></td><td></td></tr>';
                } else {
                    for (var i = 0; i < data.length; i++) {
                        var bike = data[i].bike == 1 ? '<span class="badge badge-pill badge-success">Yes</span>' : '<span class="badge badge-pill badge-danger">No</span>';
                        var car = data[i].car == 1 ? '<span class="badge badge-pill badge-success">Yes</span>' : '<span class="badge badge-pill badge-danger">No</span>';
                        var house = data[i].house == 1 ? '<span class="badge badge-pill badge-success">Yes</span>' : '<span class="badge badge-pill badge-danger">No</span>';
                        var insurance = data[i].insurance == 1 ? '<span class="badge badge-pill badge-success">Yes</span>' : '<span class="badge badge-pill badge-danger">No</span>';
                        var license = data[i].license == 1 ? '<span class="badge badge-pill badge-success">Yes</span>' : '<span class="badge badge-pill badge-danger">No</span>';
                        html += '<tr><td>'+j+'</td><td>'+data[i].company_name+'</td><td>'+house+'</td><td>'+car+'</td><td>'+bike+'</td><td>'+license+'</td><td>'+insurance+'</tr>';
                        j++;
                    }
                }
                $('#companyTable Tbody').append(html);
            }
        })
    }
    
    
</script>
@endsection