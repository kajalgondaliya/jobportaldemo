<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
	$table = 'company';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name', 'house', 'car','bike', 'license', 'insurance',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
