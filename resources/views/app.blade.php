<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Dashboard Template for Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">
    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Job Portal</a>
      <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search" id="seachInp" name="seachInp" onkeyup="onchangeSearch()">
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="#">Tecocraft</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">

            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Filters</span>
              <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
              </a>
            </h6>
            <form id="SearchForm" onchange="onchangeSearch()">
                <ul class="nav flex-column">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">
                      <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="house" name="house" value="" aria-label="..."> Apartment or House
                      </div>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active" href="#">
                      <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="car" name="car" value="option1" aria-label="..."> Car
                      </div>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active" href="#">
                      <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="bike" name="bike" value="option1" aria-label="..."> Scooter or a Bike
                      </div>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active" href="#">
                      <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="license" name="license" value="option1" aria-label="..."> Driving license
                      </div>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active" href="#">
                      <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="insurance" name="insurance" value="option1" aria-label="..."> Insurance
                      </div>
                    </a>
                  </li>
                </ul>
            </form>
          </div>
        </nav>
         @yield('content')
        </div>
    </div>
  </body>
</html>
